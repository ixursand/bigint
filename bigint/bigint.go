package bigint

import "strconv"


type Bigint struct {
	Value int
}

func Newint(num string) (*Bigint, error) {
	bigint := Bigint{}
	err := bigint.Set(num)
	if err != nil {
		return nil, err
	}
	return &bigint, nil
}

func (z *Bigint) Set(num string) error {
	response, err := strconv.Atoi(num)
	if err != nil {
		return err
	}
	z.Value = response
	return nil
}

func Add(a, b Bigint) Bigint {
	//
	// MATH ADD
	//
	return Bigint{Value: a.Value + b.Value}
}

func Sub(a, b Bigint) Bigint {
	//
	// MATH SUB
	//
	return Bigint{Value: ""}
}

func Multiply(a, b Bigint) Bigint {
	//
	// MATH Multiply
	//
	return Bigint{Value: ""}
}

func Mod(a, b Bigint) Bigint {
	//
	// MATH Mod
	//
	return Bigint{Value: ""}
}
